from setuptools import find_packages, setup

setup(
    name='ia_tools_daily_task_generator',
    version='0.1.0a1',
    packages=find_packages(),
    url='',
    license='MIT',
    author='BFG-Group',
    author_email='saa@bfg-soft.ru',
    description='Various IA web application tools.',
    install_requires=[
        'PyYAML',
        'requests',
        'openpyxl',
        'requests-toolbelt',
        'python-dateutil'
    ],
    entry_points={
        'console_scripts': ['task_generator = task_generator.schedule_analyser_via_rest:main']
    }
)
