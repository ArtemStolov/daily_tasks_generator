from argparse import ArgumentParser
from os import getcwd
from os.path import join
from task_generator.library import yml_config, xlsx_writer, analyse_transactions
from datetime import datetime
import yaml


def main():
    print('Start')
    parser = ArgumentParser(
        description='Инструмент консольной генерации отчетов по результатам моделирования.'
    )

    parser.add_argument('-c', '--config', required=False,
                        default=join(getcwd(), 'schedule_analyser_rest.yml'))
    parser.add_argument('-s', '--session', required=False,
                        default=join(getcwd(), 'daily_task_last_session.yml'))

    args = parser.parse_args()
    print(args)

    session = analyse_transactions.TransactionSession()
    cur_config = yml_config.ScheduleAnalyserRestConfig(args.config)

    if cur_config.rules["session"] == "main":

        try:
            stream = open(args.session, 'r', encoding="utf-8")
            last_session = yaml.load(stream)["session"]
        except FileNotFoundError:
            last_session = ""

        main_session = session.get_main_session(url=cur_config.input["url"],
                                                login=str(cur_config.input["login"]),
                                                password=str(cur_config.input["password"]))

        if main_session == last_session:
            print("Сессия моделирования осталась той же, ССЗ не будет сгенерировано")
            return

        if main_session is None:
            print("В системе нет принятого расчета! ССЗ не будет сгенерировано")
            return

    session.read_from_rest(url=cur_config.input["url"],
                           login=str(cur_config.input["login"]),
                           password=str(cur_config.input["password"]),
                           tz=cur_config.rules["from"].tzinfo,
                           session=cur_config.rules["session"])

    date_from = cur_config.rules["from"]
    date_to = cur_config.rules["to"]
    tasks_from = cur_config.rules["tasks_from"]
    tasks_period = cur_config.rules["tasks_period"]

    plan = sorted(session.get_plan(date_from, date_to), key=lambda i: i["DATE"])
    xlsx_writer.xlsx_write_data(xlsx=cur_config.output_plan.file,
                                worksheet=cur_config.output_plan.worksheet, data=plan)

    equipment_machinetime_report = sorted(session.daily_equipment_machinetime_report(date_from, date_to),
                                          key=lambda i: i["EQUIPMENT_ID"])

    xlsx_writer.xlsx_write_data(xlsx=cur_config.output_equipment_machinetime.file,
                                worksheet=cur_config.output_equipment_machinetime.worksheet,
                                data=equipment_machinetime_report, title="Прогноз работы оборудования в часах")

    launch_report = sorted(session.launch_report(date_from, date_to,
                                                 period=cur_config.output_production_launch.period),
                           key=lambda i: i["PRODUCT"])
    xlsx_writer.xlsx_write_data(xlsx=cur_config.output_production_launch.file,
                                worksheet=cur_config.output_production_launch.worksheet, data=launch_report)

    release_report = sorted(session.release_report(date_from, date_to,
                                                   period=cur_config.output_production_release.period),
                            key=lambda i: i["PRODUCT"])
    xlsx_writer.xlsx_write_data(xlsx=cur_config.output_production_release.file,
                                worksheet=cur_config.output_production_release.worksheet, data=release_report)

    equipment_report = sorted(session.equipment_report(date_from, date_to), key=lambda i: i["EQUIPMENT_ID"])
    xlsx_writer.xlsx_write_data(xlsx=cur_config.output_equipment.file,
                                worksheet=cur_config.output_equipment.worksheet, data=equipment_report)

    product_report = sorted(session.product_report(date_from, date_to), key=lambda i: i["PRODUCT_ID"],
                            reverse=True)
    xlsx_writer.xlsx_write_data(xlsx=cur_config.output_product.file,
                                worksheet=cur_config.output_product.worksheet, data=product_report)
    overwrite = {}
    all_reports = {}
    if tasks_from == "AUTO":
        tasks_from = None
    for equipment in session.equipment:
        tasks_report = session.daily_tasks_report(date_from=tasks_from, tasks_period_hours=tasks_period,
                                                  equipment_id=equipment)
        if tasks_report:
            department_id = session.get_equipment_with_id(equipment).department_id
            if department_id not in all_reports:
                all_reports[department_id] = {}
            all_reports[department_id][equipment] = tasks_report
            xlsx = str(cur_config.output_tasks.file) + str(department_id) + ".xlsx"
            print(session.start_time)
            if not(department_id in overwrite):
                overwrite[department_id] = True
            xlsx_writer.xlsx_write_data(xlsx=xlsx, worksheet=str(equipment)[0:30],
                                        data=tasks_report, title="ПРОИЗВОДСТВЕННОЕ ЗАДАНИЕ: " + str(equipment),
                                        overwrite=overwrite[department_id])
            if tasks_from is None:
                xlsx = str(cur_config.output_tasks.file) + "archive/" + str(datetime.now())[0:10] + "_" + \
                       str(department_id) + ".xlsx"
            else:
                xlsx = str(cur_config.output_tasks.file) + str(tasks_from)[0:10] + "_" + \
                       str(department_id) + ".xlsx"
            xlsx_writer.xlsx_write_data(xlsx=xlsx, worksheet=str(equipment)[0:30],
                                        data=tasks_report, title="ПРОИЗВОДСТВЕННОЕ ЗАДАНИЕ: " + str(equipment),
                                        overwrite=overwrite[department_id])

            overwrite[department_id] = False

    if cur_config.rules["session"] == "main":
        with open(args.session, 'w') as outfile:
            yaml.dump({"session": main_session}, outfile, default_flow_style=False)


if __name__ == "__main__":
    main()
