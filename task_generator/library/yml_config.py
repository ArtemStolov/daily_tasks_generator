import yaml
from dateutil import parser


class ConfigChapter(object):

    def __init__(self, data, columns=()):
        self.columns = {}
        self.file = data["file"]

        if "worksheet" in data:
            self.worksheet = data["worksheet"]
        else:
            self.worksheet = "Sheet 1"

        if "period" in data:
            self.period = data["period"]

        if "columns" in data:
            data_columns = data["columns"]
            for key in columns:
                self.columns[key] = data_columns[key]


class ScheduleAnalyserRestConfig(object):

    def __init__(self, config):
        with open(config, 'r', encoding="utf-8") as stream:
            data = yaml.load(stream)

        self.input = data["input"]
        self.output_product = ConfigChapter(data["output_product"])
        self.output_equipment = ConfigChapter(data["output_equipment"])
        self.output_equipment_machinetime = ConfigChapter(data["output_equipment_machinetime"])
        self.output_production_launch = ConfigChapter(data["output_production_launch"])
        self.output_production_release = ConfigChapter(data["output_production_release"])
        self.output_plan = ConfigChapter(data["output_plan"])
        self.output_tasks = ConfigChapter(data["output_tasks"])
        self.rules = data["rules"]
        self.rules["from"] = parser.parse(self.rules["from"])
        self.rules["to"] = parser.parse(self.rules["to"])
        try:
            self.rules["tasks_from"] = parser.parse(self.rules["tasks_from"])
        except ValueError:
            print("Значение {} в поле rules|tasks_from не "
                  "удалось распознать в качестве даты".format(self.rules["tasks_from"]))
